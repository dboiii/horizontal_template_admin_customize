module LayoutCustomizeHelper
  def active_btn_customize(arr_controller_n)
    arr_controller_n.include?(controller_name) ? "active" : ""
  end

  def title_header_helper(data)
    if data.present? && data == "default" || !data.present?
      return "Dashboard"
    else
      return data
    end
  end

  def url_header_helper(data)
    if data.present? && data == "default" || !data.present?
      return "admin/shared/customize_partial/header"
    else
      return data
    end
  end

  def url_navbar_helper(data)
    if data.present? && data == "default" || !data.present?
      return "admin/shared/customize_partial/navbar"
    else
      return data
    end
  end

  def url_breadcrumb_helper(data)
    if data.present? && data == "default" || !data.present?
      return "admin/shared/customize_partial/breadcrumb"
    else
      return data
    end
  end

  def url_flash_message_helper(data)
    if data.present? && data == "default" || !data.present?
      return "admin/shared/customize_partial/flash_message"
    else
      return data
    end
  end

  def url_footer_helper(data)
    if data.present? && data == "default" || !data.present?
      return "admin/shared/customize_partial/footer"
    else
      return data
    end
  end
end
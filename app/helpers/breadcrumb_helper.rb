module BreadcrumbHelper
  def define_li
    arr = []
    if action_name == "index"
      arr << "<li class='breadcrumb-item active'>#{controller_name.titleize}</li>"
    else
      arr << "<li class='breadcrumb-item'><a href='/admin/#{controller_name}'>#{controller_name.titleize}</a></li>"
      arr << "<li class='breadcrumb-item active'>#{action_name.titleize}</li>"
    end
    return arr.join(" ").html_safe
  end
end
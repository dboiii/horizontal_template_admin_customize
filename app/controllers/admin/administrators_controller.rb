class Admin::AdministratorsController < Admin::BaseController
  before_action :set_admin, only: %i(edit update)

  def edit; end

  def update
    if @administrator.update_attributes(admin_params)
      redirect_to admin_root_path, notice: t('notices.administrator.updated').freeze
    else
      render :edit
    end
  end

  private

    def set_admin
      @administrator = Administrator.find(params[:id])
    end

    def admin_params
      if params[:administrator][:password].blank? && params[:administrator][:password_confirmation].blank?
        params.require(:administrator).permit(:email)
      else
        params.require(:administrator).permit(:email, :password, :password_confirmation)
      end
    end

end
